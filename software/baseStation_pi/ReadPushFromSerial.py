## Importing required Modules
import serial
import sys
import time
from datetime import datetime
#import os, sys
import psycopg2
from config import config


## fetch data from receiver

port = '/dev/ttyACM0'
baudrate = 9600

ser = serial.Serial(port, baudrate)
a = True
while a:
    #### process the received data
    #print( str(datetime.now()) )
    rawData_str = []
    rawData_strList = []
    # data processing
    rawData_str = ser.readline().strip()
    #print('raw data is ' + rawData_str+ '\n')
    rawData_strList = rawData_str.split(',')
    if len(rawData_strList)==9:
        uid, Temp, ReHu, voc, comfort, junk, co2, light, occupancy = [float(_) for _ in rawData_strList]
        uid = int(uid)
        currentTime = datetime.now()
        Insert_Data = [uid, currentTime, Temp, ReHu, voc, comfort, co2, light, occupancy]
        print('The data to be inserted is: ' + str(Insert_Data))
        
        #### push data to database
        conn = None # initialize connection
        try:
            #### Connecting to the Database
            params = config() # read connection parameters
            #print(params) # Printing Parameters

            # connect to the PostgreSQL server
            #print('Connecting to the PostgreSQL database...')
            conn = psycopg2.connect(**params)
            #print("Connection has been made treadlineo the Database!!")

            ## Test2: Getting Database Information

            # create a cursor
            cur = conn.cursor()

            # Database Table Names
            TableName="pcndata"

            # Database Column Names
            ColumnName_String=" \"uid\",\"date_time\",\"temperature\",\"humidity\",\"voc\",\"comfort\",\"co2\",\"light\",\"occupancy\" "

            # Value String
            Value_String="%s,%s,%s,%s,%s,%s,%s,%s,%s"

            # Creating Insert Query for Table
            SQL_Query="INSERT INTO "+ "\""+TableName +"\"" +" ("+ ColumnName_String  + ")" +" VALUES ("+ Value_String +")"
            
            # execute the INSERT statement
            cur.execute(SQL_Query,Insert_Data)
            
            # commit the changes to the database
            conn.commit()
            
            #close the communication with the PostgreSQL
            cur.close()

            print("The data from PCN "+str(Insert_Data[0])+ " at time "+ str(currentTime) + " is inserted into the database")

            time.sleep(0.002)

            print("\n")


        except (Exception, psycopg2.DatabaseError) as error:
                print(error)
            
    #### if data received is corrupted
    else:
        conn = None
        print('raw data is corrupted: ' + str(rawData_strList))

    
#close the connection
if conn is not None:
    conn.close()
    print('Database connection closed.' + "\n")


    
