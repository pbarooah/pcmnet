  
/*  PCFN, REVISION M
 *  Lucas Murphy, Joseph Rosenberger, Austin Coffman, Prabir Barooah
 *  DiCE Lab, University of Florida
 */

 
//SET THIS NUMBER TO THE DEVICE ID
int deviceID = 8;

//*****VOC SENSOR SETUP**
int vocpin = A14;

//*****light SENSOR SETUP**
int lightpin = A15;

//*****PIR SENSOR SETUP (occupancy sensor)*****
int power = 5;
int inputPir = 23;               // choose the input pin (for PIR sensor)
int printPayload = 1;
int occupantStatusSend = 0;

//*****TOUCH SCREEN SETUP*****

#include <Adafruit_GFX.h>    // Core graphics library
#include <SPI.h>       // this is needed for display
#include <Adafruit_ILI9341.h>
#include <Wire.h>      // this is needed for FT6206
#include <Adafruit_FT6206.h>

#define BLACK   0x0000
#define BLUE    0x001F
#define RED     0xF800
#define GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF
#define SAND    0xDEE0
#define ICE     0x075D
#define ORANGE  0xFA22
#define GBLUE   0x0114

// perhaps explanation for this:
int TempIndex = 0;
int HumidIndex = 0;
int TempNum = 0;
int HumidNum = 0;
int TempNumOld = 0;
int HumidNumOld = 0;


// This object names need to be renamed, what is the meanig of ctp?:
// The FT6206 uses hardware I2C (SCL/SDA)
Adafruit_FT6206 ctp = Adafruit_FT6206();

// The display also uses hardware SPI, plus #9 & #10
#define TFT_CS 10
#define TFT_DC 9
// This object names need to be renamed, what is the meanig of tft?: (touchScreen)
Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC);

//*************CO2 SETUP*********
#include "kSeries.h" //include kSeries Library
kSeries K_30(13,12); //Initialize a kSeries Sensor with pin 12 as Rx and 13 as Tx


//*****TEMPERATURE SENSOR SETUP*****

#include <OneWire.h>
#include <DallasTemperature.h>

// Specify pin number that data wire is connected to (pin 2)
#define ONE_WIRE_BUS 25

//Creates oneWire instance to communicate with all OneWire devices on the bus specified above
OneWire oneWire(ONE_WIRE_BUS);

//Passes oneWire reference to Dallas Temperature
DallasTemperature sensors(&oneWire);

//*****HUMIDITY SENSOR SETUP*****

#include <SparkFun_HIH4030.h>

#define HIH4030_OUT A13
#define HIH4030_SUPPLY 5
HIH4030 sensorSpecs(HIH4030_OUT, HIH4030_SUPPLY);

//*****XBEE SETUP*****

#include <XBee.h>
#include <Printers.h>

XBeeWithCallbacks xbee;
uint8_t payload[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }; // integers decimals separate
XBeeAddress64 coord = XBeeAddress64(0x0013a200, 0x415CC1F5);  //address for basestation
ZBTxRequest zbtx = ZBTxRequest(coord, payload, sizeof(payload));

//Setting up counter for periodic actions
unsigned long previousMillis = 0; 
const long interval = 10000;

//Status object:
ZBTxStatusResponse txStatus = ZBTxStatusResponse();  

// Setup of the Arduino:
//===============================================================
void setup() {

  // PIR Setup
  pinMode(power, OUTPUT);
  pinMode(inputPir, INPUT);     // declare sensor as input
    
  // serial Setup
  Serial.begin(9600);
  Serial1.begin(9600);

  // Xbee setup
  xbee.setSerial(Serial1);

  // Temp sensor setup
  sensors.begin();
  
  //TOuch screen setup
  digitalWrite(power, HIGH);       // sets the digital pin 13 on
  tft.begin();

    // cant start the touch screen, so turn it off:
  if (!ctp.begin(40)) {  // pass in 'sensitivity' coefficient
      Serial.println("Couldn't start FT6206 touchscreen controller");
      digitalWrite(power, LOW);
      while (1);
  }

    // continue with touch screen setup:
    Serial.println("Capacitive touchscreen started");
    tft.setRotation(1);
    titleScreen();
    delay(1000);
    tft.fillScreen(BLACK);
    drawSlider();
    delay(1000);

// setup VOC sensor:
// This is due to Tim Martin, "
// "I reworked the enable circuit compared to the Adafruit breakout 
// board, and I utilized an unused IO pin to accomplish the enable
    pinMode(27, OUTPUT);
    digitalWrite(27,HIGH);

}
//===============================================================


// Main Arduino Loop:
//===============================================================
void loop() {
    // Retrieve a touch screen point "p":  
    TS_Point p = ctp.getPoint();
    
  // flip it around to match the screen:
    p.x = map(p.x, 0, 240, 240, 0);
    p.y = map(p.y, 0, 320, 320, 0);
  
    // sliding the comfort bar:
  if ( (p.x > 170) && (p.x < 210) && (p.y > 30) && (p.y < 290) ) {
      
      // filling the bar color:
      tft.fillRect(21,31,(p.y - 10) - 21,38,BLACK);
      tft.fillRect(p.y+10,31,299 - (p.y + 10),38,BLACK);
      tft.fillRect((p.y) - 10,30,20,40, WHITE);

      // what does this do:
      TempIndex = p.y - 160;
      if    (TempIndex >=- 18 && TempIndex <= 19)   {TempNum = 0;}

      else if (TempIndex > 19 && TempIndex <= 56)   {TempNum = 1;} 
      
      else if (TempIndex > 56 && TempIndex <= 93)   {TempNum = 2;} 
      
      else if (TempIndex > 93 && TempIndex <= 129)  {TempNum = 3;} 
      
      else if (TempIndex <- 18 && TempIndex >= -55)   {TempNum = -1;} 
      
      else if (TempIndex <- 55 && TempIndex >=- 92)   {TempNum = -2;} 
      
      else if (TempIndex <- 92 && TempIndex >=- 129)  {TempNum = -3;}

      // filling the bar color:
      if (TempNum != TempNumOld) {
          tft.fillRect(140,2,100,25,BLACK);
          tft.setCursor(145,5);
          tft.setTextSize(3);
          tft.setTextColor(GREEN);
          tft.print(TempNum);
          TempNumOld = TempNum;
      }

  // "update" and send user input:
    } else if ( (p.y > 220) && (p.y < 300) &&  (p.x > 80) && (p.x < 160) ) {

      // what does this do:
      tft.fillRect(0,70,230,80,BLACK);
      tft.setCursor(20,110);
      tft.setTextSize(4);
      tft.setTextColor(ORANGE);
      tft.print("Updated!");
      tft.fillCircle(260,120,25,MAGENTA);

      // get sensor data and populate payload:
      occupantStatusSend = (int) digitalRead(inputPir);
      populatePayload(payload, vocpin, inputPir, lightpin, sensorSpecs, deviceID, printPayload, occupantStatusSend);
      
      // send the data:
      //sendData(zbtx);
      xbee.send(zbtx);
      
      //What does this do:
      tft.fillRect(0,70,230,80,BLACK);
      tft.fillCircle(260,120,25,GBLUE);
      tft.setCursor(20,110);
      tft.setTextSize(4);
      tft.setTextColor(BLUE);
      tft.println("Update:");

    }

    
  //Send the data regardless of occupant interaction:
    unsigned long currentMillis = millis(); //PB: repeated?


  if ((unsigned long)(currentMillis - previousMillis) >= interval) {

      // reset values:
      previousMillis = currentMillis;

      // get sensor data and populate payload:
      occupantStatusSend = (int) digitalRead(inputPir);
      populatePayload(payload, vocpin, inputPir, lightpin, sensorSpecs, deviceID, printPayload, occupantStatusSend);
        
      //send the data:
      //sendData(zbtx); 
      xbee.send(zbtx);
      
      // print the payload to serial port: 
    //printPayload(payload);
  }

}
//===============================================================


//*****FUNCTIONS*****
//===============================================================

// function to send data with feedback control:
void sendData(ZBTxRequest transObj) {
  
  // construct booleans for flow control:
  bool boolValue = true;
  bool innerBool = true;

  // set maximum interation integers:
  int maxIter = 5;
  int countIter = 0;

  // loop for sending:
  while (boolValue) {

    // initial send of object:
    xbee.send(transObj);

    // look for status response:
    while (innerBool) {
      // read a packet:
      xbee.readPacket(2000);

      // check if packet is what we want:
      if (xbee.getResponse().getApiId() == ZB_TX_STATUS_RESPONSE) {
        innerBool = false;
      }

    } // end of looking for correct response.

    // now populate the xbee TX status response:
    xbee.getResponse().getZBTxStatusResponse(txStatus);

    // ensure a success (sends again if not success):
    if ( (txStatus.getDeliveryStatus() == SUCCESS) || (countIter >= maxIter) ) {
      boolValue = false;  
    }

    // increment counter:
    countIter++;
  }

  // return the bool value (redundent for now):
  //return ~boolValue;

}

// function to set the title screen for the touch screen:
unsigned long titleScreen() {
  tft.fillScreen(BLACK);
  tft.setCursor(40,40);
  tft.setTextSize(3);
  tft.setTextColor(WHITE);
  tft.println("Environmental\n   Control Unit");
  tft.print("\n\nStarting now");
  delay(500);
  tft.print(".");
  delay(500);
  tft.print(".");
  delay(500);
  tft.print(".");
  delay(500);
  tft.print(".");
  delay(500);
  tft.print(".");
  
}

// function to draw slider for the touch screen:
unsigned long drawSlider() {
  tft.drawRect(20,30,280,40, WHITE);
  //tft.drawRect(20,200,280,40, WHITE);
  tft.setCursor(20,10);
  tft.setTextSize(2);
  tft.setTextColor(ICE);
  tft.print("COLD");
  tft.setCursor(265,10);
  tft.setTextColor(RED);
  tft.print("HOT");
    
  tft.fillRect(150,30,20,40,WHITE);
  //tft.fillRect(150,200,20,40,WHITE);
  tft.setCursor(145,5);
  tft.setTextSize(3);
  tft.setTextColor(GREEN);
  tft.print(TempNum);
  tft.setCursor(145,170);
  
  tft.fillCircle(260,120,30,ORANGE);
  tft.fillCircle(260,120,25,GBLUE);
  tft.setCursor(20,110);
  tft.setTextSize(4);
  tft.setTextColor(BLUE);
  tft.println("Update:");
  
}

// function to extract the humidity data:
float HumidData(HIH4030 sensor, float temperature) {
  float humidity = sensor.getTrueRH(temperature);
  return humidity;

}


void populatePayload(uint8_t payload[], int vocpin, int inputPir, int lightpin, HIH4030 sensorSpecs, int deviceID, int printPayload, int occupantStatusSend) {
      
      sensors.requestTemperatures();    
      float tempRAW = sensors.getTempFByIndex(0);
      int tempA = (int) tempRAW;
      int tempB = tempRAW*100 - tempA*100;
      
      float humidRAW = HumidData(sensorSpecs, sensors.getTempCByIndex(0));
      int humidA = (int) humidRAW;
      int humidB = humidRAW*100 - humidA*100;

      float VOCraw = analogRead(vocpin);
      float VOC = (VOCraw/1024)*100;
      int VOCA = (int) VOC;
      int VOCB = VOC*100 - VOCA*100;

      int comfort = TempNumOld;
      
      double co2 = K_30.getCO2('p'); //returns co2 value in ppm ('p') or percent ('%')
      int co2A = (int) (co2/100);
      int co2B = co2 - co2A*100;
      
      float lightraw = analogRead(lightpin); //light dependent resistor so units in ohms
      int lightrawA = (int) (lightraw/100);
      int lightrawB = lightraw - lightrawA*100;
      

      payload[0] = deviceID;
      payload[1] = tempA; // integers in temperature
      payload[2] = tempB; // decimals in temperature
      payload[3] = humidA;// integers in humidity
      payload[4] = humidB;// decimals in humidity
      payload[5] = VOCA;  // integers in VOC
      payload[6] = VOCB;  // decimals in VOC
      payload[7] = comfort;
      payload[8] = 20;
      payload[9] = co2A;
      payload[10] = co2B;
      payload[11] = lightrawA;
      payload[12] = lightrawB;
      payload[13] = occupantStatusSend;

      if (printPayload == 1) {
        Serial.print("temperature (raw) = ");        
        Serial.println(tempRAW);
        Serial.print("rel. hum (%, raw) = ");        
        Serial.println(humidRAW);
        Serial.print("VOC concentration (ppm?) = ");        
        Serial.println(VOC);
        Serial.print("comfort = ");        
        Serial.println(comfort);
        Serial.print("co2 (ppm) = ");        
        Serial.println(co2);
        Serial.print("light level (raw) = ");        
        Serial.println(lightraw);
        Serial.print("occupancy status = ");        
        Serial.println(occupantStatusSend);
        Serial.print("UID = ");        
        Serial.println(deviceID);
        Serial.println("============");
             
        }

  
  }

//===============================================================
