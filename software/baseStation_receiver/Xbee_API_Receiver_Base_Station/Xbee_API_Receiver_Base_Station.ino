
  
/* ENVIRONMENTAL CONTROL UNIT, REVISION I
 *  Lucas Murphy, Joseph Rosenberger, Austin Coffman, Ninad Gaikwad, Srajat Rastogi, Dr. Prabir Barooah
 *  DiCE Lab, University of Florida
 */

/* Changes made from REV- to REV I:
 *    Included occupancy status data packet
 */

#include <XBee.h>
XBee xbee = XBee();
ZBRxResponse zbrx = ZBRxResponse();
ModemStatusResponse msr = ModemStatusResponse();


void setup() {
  Serial.begin(9600);
  Serial1.begin(9600);
  xbee.begin(Serial1);
}

void loop() {
  xbee.readPacket();
//  Serial.print(xbee.getResponse().isAvailable());
  if (xbee.getResponse().isAvailable()) {
    // got something
    
    if (xbee.getResponse().getApiId() == ZB_RX_RESPONSE) {
      // got a zb rx packet
      
      // now fill our zb rx class
      xbee.getResponse().getZBRxResponse(zbrx);
      int ID = zbrx.getData(0);
      float t1 = zbrx.getData(1);
      float t2 = zbrx.getData(2);
      float temp = t1 + t2/100;
      float h1 = zbrx.getData(3);
      float h2 = zbrx.getData(4);
      float humid = h1 + h2/100;
      float v1 = zbrx.getData(5);
      float v2 = zbrx.getData(6);
      float voc = v1 + v2/100;
      int tnum = zbrx.getData(7);
      int hnum = zbrx.getData(8);
      float co2A = zbrx.getData(9);
      float co2B = zbrx.getData(10);
      float lightrawA = zbrx.getData(11);
      float lightrawB = zbrx.getData(12);
      float occupancyStatus = zbrx.getData(13);
      
      float co2 = co2B + co2A*100;
      float lightraw = lightrawB + lightrawA*100;
      if (tnum > 240){
        tnum = (256 - tnum)*-1;
      }
      if (hnum > 240){
        hnum = (256 - hnum)*-1;
      }
      
      //Serial.print("UID ");
      Serial.print(ID);
      Serial.print(",");
      //Serial.print("TEMPERATURE_SENSOR-3 ");
      Serial.print(temp);
      Serial.print(",");
      //Serial.print("HUMIDITY_SENSOR-4 ");
      Serial.print(humid);
      Serial.print(",");
      //Serial.print("VOC_SENSOR-5 ");
      Serial.print(voc);
      Serial.print(",");
      //Serial.print("TEMPERATURE_HUMAN-6 ");
      if ((tnum > 10) && (tnum < 240)){
        Serial.print("NaN");
      } else {
        Serial.print(tnum);
      }
      Serial.print(",");
      //Serial.print("HUMIDITY_HUMAN-7 ");
      if ((hnum > 10) && (hnum < 240)){
        Serial.print("NaN");
      } else {
        Serial.print(hnum);
      }
      Serial.print(",");
      //Serial.print("CO2_SENSOR-8 ");
      Serial.print(co2);
      Serial.print(",");
      //Serial.print("LIGHT_SENSOR-9 ");
      Serial.print(lightraw);
      Serial.print(",");
      //Serial.print("OCCUPANCY_STATUS-10 ");
      Serial.print(occupancyStatus);
      Serial.print("\n"); 
   
      if (zbrx.getOption() == ZB_PACKET_ACKNOWLEDGED) {
          // the sender got an ACK
          //Serial.print("Sender got an ACK");
      } else {
          // we got it (obviously) but sender didn't get an ACK
          //Serial.print("Sender didn't get an ACK");
      }
      // set dataLed PWM to value of the first byte in the data
//      analogWrite(dataLed, zbrx.getData(0));
 
    } else if (xbee.getResponse().getApiId() == MODEM_STATUS_RESPONSE) {
        xbee.getResponse().getModemStatusResponse(msr);
        // the local XBee sends this response on certain events, like association/dissociation
        
        if (msr.getStatus() == ASSOCIATED) {
          // yay this is great.  flash led
          //Serial.print("Associated");
        } else if (msr.getStatus() == DISASSOCIATED) {
          // this is awful.. flash led to show our discontent
          //Serial.print("Disassociated");
        } else {
          // another status
          //Serial.print("Other");
        }
    } else {
      // not something we were expecting
      //Serial.print("unexpected ");
    }
  } else if (xbee.getResponse().isError()) {
    //Serial.print("Error reading packet.  Error code: ");  
    //Serial.println(xbee.getResponse().getErrorCode());
  }


  
      

} 




      
