# pcmnet

PCIEM-Net: (read as p-c-m net) a system for Personal Comfort and Indoor Environment Monitoring, with participatory-sensing-enabled wireless nodes and database management. 


## Description

The PCIEM-Net is a network of devices (each called a Personal Comfort Feedback Node - PCFN) that collects indoor environmental data from sensors and enables office occupants to provide feedback on their perceived comfort through  a touchscreen. Details about the system are described in the pdf file in the doc>paper folder. 



## Authors and acknowledgment
The majority of the hardware development was carried out by Joseph Rosenberger, with help from Austin Coffman and Zhong Guo. We thank Lucas Murphy for his help with the first PCFN prototype, Roshan Raisoni and Ninad Gaikwad for their help with writing the server side scripts and testing. 

The project was supported partially by a grant from the Florida Department of Agricultural and Consumer Services and NSF awards 1646229 (CPS/ECCS) and 1934322 (CMMI).

## License
MIT license

## Project status
Development has been paused.
